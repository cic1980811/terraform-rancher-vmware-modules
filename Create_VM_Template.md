# Create a VM Template on vSphere

## For Ubuntu OS

You can easily upload an OVF/OVA template:

- Right-click any inventory object that is a valid parent object of a virtual machine, such as a data center, folder, cluster, resource pool, or host, and select `Deploy OVF Template`.
The Deploy OVF Template wizard opens.

For Ubuntu, you can find pre-built templates from this URL <https://cloud-images.ubuntu.com/releases/>. For example, you can download [this Ubuntu 21.04 OVA image](https://cloud-images.ubuntu.com/releases/hirsute/release/ubuntu-21.04-server-cloudimg-amd64.ova).

You can now right-click on the VM named something like `ubuntu-21.04-server-cloudimg-amd64` and start it. Do a first login with user `ubuntu` and the password you set when deploying the OVF/OVA template in your vCenter folder.

Set the keyboard to french with command `sudo loadkeys fr`.

And configure permanent french keyboard settings with `sudo dpkg-reconfigure keyboard-configuration`.

It is advised to launch also the following commands to avoid losing time for packages upgrade and installation of docker:

```shell
sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo apt-get upgrade -y
```

## For Suse OS

You can download a VMDK compressed disk file on [this web site](https://www.suse.com/download/sles/). You must first login with your Orange customer account.

Uncompress the file with this command: `xz -d SLES15-SP3-JeOS.x86_64-15.3-VMware-GM.vmdk.xz`.

Then upload the file (for example, `SLES15-SP3-JeOS.x86_64-15.3-VMware-GM.vmdk.xz`) on a Datastore.

Then you must create a new VM, with a guest OS `Suse Linux Enterprise 15 (64 bits)` and when you arrive in the `Customize hardware` section, click on the `ADD NEW DEVICE` menu and select `Existing Hard Disk`. Browse the datastore and indicate the VMDK file that you've just uploaded in the previous step. 

