variable "rancher_api_url" {
  description = "The Access Key giving access to the Rancher server"
  type        = string
  default     = "https://rancher.edge.inter-cdnrd.orange-business.com"
}

variable "rancher2_access_key" {
  description = "The Access Key giving access to the Rancher server"
  type        = string
}

variable "rancher2_secret_key" {
  description = "The Secret Key giving access to the Rancher server"
  type        = string
}

variable "insecure_rancher_server" {
  description = "Flag that indicates whether the Rancher server uses an auto-signed certificate for TLS or not"
  type        = bool
  default     = true
}

variable "vsphere_server" {
  description = "The FQDN of the vSphere server"
  type        = string
}

variable "vsphere_user" {
  description = "The user name for connecting to the vSphere server"
  type        = string
}

variable "vsphere_password" {
  description = "The password for connecting to the vSphere server"
  type        = string
}

variable "default_vm_user_password" {
  description = "The default shell user to create on VMs"
  type        = string
  sensitive   = true
}