# Rancher credentials
rancher_api_url         = "https://rancher.edge.inter-cdnrd.orange-business.com"
rancher2_access_key     = "<your-rancher-access-token>"
rancher2_secret_key     = "<your-rancher-secret>"
insecure_rancher_server = true

# vSphere settings
vsphere_server   = "192.168.124.143"
vsphere_user     = "<your-vsphere-user-name>"
vsphere_password = "<your-vsphere-password"

# Some VM secrets
default_vm_user_password = "your-vm-default-user-password"