# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DEPLOY A RANCHER CLUSTER USING THE K3S MODULE
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

terraform {
  required_version = ">= 1.1.0"

  required_providers {
    rancher2 = {
      source  = "rancher/rancher2"
      version = "1.23.0"
    }

    vsphere = {
      source  = "hashicorp/vsphere"
      version = "2.1.1"
    }
  }
}

# ------------------------------------------------------------------------------
# CONFIGURE OUR VSPHERE CONNECTION
# ------------------------------------------------------------------------------

provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

# ------------------------------------------------------------------------------
# CONFIGURE OUR RANCHER CONNECTION
# ------------------------------------------------------------------------------

provider "rancher2" {
  # Configuration options
  api_url    = var.rancher_api_url
  access_key = var.rancher2_access_key
  secret_key = var.rancher2_secret_key
  insecure   = var.insecure_rancher_server
}

# ------------------------------------------------------------------------------
# DEPLOY THE K3S CLUSTER MODULE USING TERRAFORM to INSTANTIATE VMs ON VSPHERE
# ------------------------------------------------------------------------------

module "k3s_cluster" {
  source = "./clusters/k3s_with_terraform_vsphere_provider"

  # Rancher settings
  insecure_rancher_server = true

  cluster_name = "test-k3s"
  labels       = { "provider" = "vsphere", "type" = "k3s" }
  k8s_version  = "v1.22.9+k3s1"

  # vCenter settings
  vsphere_datacenter    = "EdgeDC"
  vsphere_folder        = "/EdgeDC/vm/Devops"
  vsphere_datastore     = "DS"
  vsphere_resource_pool = "TF"
  vsphere_network       = "EDGE-VLAN"
  //vm_template_name      = "template-vm"
  vm_template_name = "ubuntu-21.04-server-cloudimg-amd64"

  # VM settings
  vm_count = 3
  //vm_iso_image   = "CentOS-8.3.2011-x86_64-dvd1.iso"
  vm_iso_image        = "ubuntu-20.04.2-live-server-amd64.iso"
  vm_guest_os_id      = "ubuntu64Guest"
  vm_memory_size      = 4
  vm_nb_cpu           = 2
  vm_system_disk_size = 40
  //dns_server_list = ["192.168.1.57", "8.8.8.8"]
  dns_server_1             = "192.168.1.57"
  dns_server_2             = "8.8.8.8"
  default_vm_user          = "local"
  default_vm_user_password = var.default_vm_user_password
  vm_ssh_authorized_key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDL2vwT429rXB4OYPVN2NHPIewRbSBdC0Cy5oMlKnSCzhAwh8Ug783NYbsEsrlXXxVhkpjiC7WpzPLWnsSRDv5XaaP1p0heCwrr+ai0kW0RydjK20LV3LK8v0R8S/ukm36cVo8oBQABZaVwpOUzjTpzUw6LSa+ybaQch96pj427xBCLhLQDJIk4LVqKx+kUGFDHzXxkJULreIVBdk1L1dCNbmUwxTyMqlPcX0jdqJ5vsLc13CLJ4Q48oROVy3OrtW3zLAu3PYnGnFoQgltG3GMdcR0T4x0ozEknHqKnE2cOlZ63dybnDDNR2AP/C5tlglLSVpNxzEs5UerBZR6zTko+kK5hB45Fy+UJn2L+Pc648/5Az0ufATVzdreWoyeXdj1mD8Ke37L8yt9uqQioZ8G5sCi4HDWYfmpR17MCugBDrhRuM3F71eYnCUaMkMHqcmV2PGI2PQ1ri/jwAb3kgkLrEfFg6fOT/cGDCxKp09hvp7CJBO6X7vGi/ti0uQSo/IHHQHbJ3Iro4PC5uuHEAp+aBDw2KPWIDX5SHAz3+qdXyCNLpjVZ7uNgmLc4buqTUrzKh0aGqkbbPeaYeQNaJBmC9OvFN2rSoHoCLhJ0hkdqUtqVBudYb76/KiLGugftM+M5wQW/vINh64BqPscj2/oAEsR5CHcsZLOQDGxLGbVzQQ== vagrant@linux"

}

/* module "k3s_cluster" {
  source = "./clusters/k3s_with_rancher_vsphere_connector"

  # Rancher settings
  insecure_rancher_server = true

  cluster_name = "vmahe6-k3s"
  labels       = { "provider" = "vsphere", "type" = "k3s" }
  //k8s_version  = "v1.21.9+k3s1"
  k8s_version = "v1.22.7+k3s1"

  # vCenter settings
  vsphere_server     = var.vsphere_server
  vsphere_user       = var.vsphere_user
  vsphere_password   = var.vsphere_password
  vsphere_datacenter = "ESXi"
  //vsphere_folder        = "ESXi"

  // When we set this value, Rancher considers this path '/ESXi/vm/ESXi/vm' !
  // and therefore triggers an error.
  //vsphere_folder        = "/ESXi/vm" 
  vsphere_folder        = null
  vsphere_datastore     = "datastore1"
  vsphere_resource_pool = null
  vsphere_network       = ["VM Network"] // Be careful, sensitive to the case of the letters.

  # VM settings
  vm_template_name    = "k3s-labo-256G"
  vm_count            = 1
  vm_memory_size      = 4
  vm_nb_cpu           = 2
  vm_system_disk_size = 40

} */
