
output "k3s_cluster_name" {
  value       = module.k3s_cluster.cluster_name
  description = "The name of the K3S cluster hosted on VMs deployed with the Terraform vsphere provider"
}

output "k3s_cluster_kube_config" {
  value       = module.k3s_cluster.cluster_kube_config
  description = "The kubeconfig file that can be used to authenticate on the cluster in order to launch some `kubectl` commands"
  sensitive   = true
}

output "k3s_cluster_vm_ips" {
  value       = module.k3s_cluster.ips
  description = "The list of IP address of the VMs that have been created on the vCenter"
}

/* output "k3s_cluster_name_with_rancher_vsphere_connector" {
  value       = module.k3s_with_rancher_vsphere_connector.cluster_name
  description = "The name of the K3S cluster instantiated with the Rancher connector for vSphere"
}

output "k3s_cluster_kube_config_with_rancher_vsphere_connector" {
  value     = module.k3s_with_rancher_vsphere_connector.cluster_kube_config
  sensitive = true
}
 */
