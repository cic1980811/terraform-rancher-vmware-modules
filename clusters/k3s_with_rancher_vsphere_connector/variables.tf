variable "insecure_rancher_server" {
  description = "Flag that indicates whether the Rancher server uses an auto-signed certificate for TLS or not"
  type        = bool
  default     = true
}

variable "cluster_name" {
  description = "The name of the cluster to deploy. If you leave it empty, a default unique name will be built automatically"
  type        = string
  default     = ""
}

variable "labels" {
  description = "The map of Kubernetes labels to be applied to the cluster"
  type        = map(any)
  default = {
    "provider" = "vsphere",
    "type"     = "k3s"
  }
}

variable "k8s_version" {
  description = "The version of the Kubernetes cluster"
  type        = string
  default     = "v1.22.7+k3s1"
}

variable "cluster_type_prefix" {
  description = "The prefix to use for the type of cluster (used to build the name of the cluster)"
  type        = string
  default     = "k3s"
}

variable "vsphere_server" {
  description = "The FQDN of the vSphere server"
  type        = string
}

variable "vsphere_user" {
  description = "The user name for connecting to the vSphere server"
  type        = string
}

variable "vsphere_password" {
  description = "The password for connecting to the vSphere server"
  type        = string
}

variable "vsphere_datacenter" {
  description = "The vSphere datacenter to use"
  type        = string
  default     = "dc1"
}

variable "vsphere_datastore" {
  description = "The vSphere datastore to use"
  type        = string
  default     = "datastore1"
}

variable "vsphere_folder" {
  description = "vSphere folder for the VMs. This folder must already exist in the vsphere datacenter"
  type        = string
  default     = "/dc1/vm/folder1"
}

variable "vsphere_resource_pool" {
  description = "The vSphere pool of resources to use"
  type        = string
  default     = "cluster1/Resources"
}

variable "vsphere_network" {
  description = "vSphere network where the docker VM will be attached. Be careful to the network name because it is case sensitive."
  type        = list(any)
  default     = ["public"]
}

variable "vm_template_name" {
  description = "The name of the vSphere VM template to use in order to instantiate the virtual machine(s)."
  type        = string
}

variable "vm_count" {
  description = "number of VMs to create"
  type        = number
  default     = 1
  validation {
    condition     = var.vm_count >= 1
    error_message = "Must be 1 or more."
  }
}

variable "vm_memory_size" {
  description = "The size of the RAM on the virtual machine (in GBytes)"
  type        = number
  default     = 4
}

variable "vm_nb_cpu" {
  description = "The number of vcpu on the virtual machine"
  type        = number
  default     = 1
}

variable "vm_system_disk_size" {
  description = "The size of the system disk on the virtual machine (in GBytes)"
  type        = number
  default     = 20
}
