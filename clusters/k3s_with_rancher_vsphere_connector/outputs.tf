output "cluster_kube_config" {
  description = "The kubeconfig that gives you access to the cluster's APIs"
  value       = rancher2_cluster_v2.cluster_on_vsphere.kube_config
  sensitive   = true
}

output "cluster_kube_config_file" {
  description = "The name of the file containing the kubeconfig for the K8S cluster"
  value       = local.cluster_kubeconfig_filename
}

output "cluster_name" {
  description = "The name of the K8S cluster"
  value       = rancher2_cluster_v2.cluster_on_vsphere.name
}

