
terraform {
  required_version = ">= 1.1.0"

  required_providers {
    rancher2 = {
      source  = "rancher/rancher2"
      version = "1.23.0"
    }
  }
}

# Create a new rancher2 Cloud Credential for vSphere
resource "rancher2_cloud_credential" "vsphere_creds" {
  name        = "vsphere_creds"
  description = "Some credentials to access a vCenter server"
  vsphere_credential_config {
    vcenter  = var.vsphere_server // vSphere IP/hostname for vCenter
    username = var.vsphere_user
    password = var.vsphere_password
  }
}

// To have a unique naming identifier for cluster and nodes, we use Random:
resource "random_id" "cluster_instance_id" {
  byte_length = 3
}

# Create vSphere machine config v2
resource "rancher2_machine_config_v2" "vsphere" {
  generate_name = "vsphere-config"

  // See parameters in https://registry.terraform.io/providers/rancher/rancher2/latest/docs/resources/machine_config_v2#vsphere_config
  vsphere_config {
    creation_type = "template" // (Optional) Creation type when creating a new virtual machine. Supported values: vm, template, library, legacy. Default legacy (string)
    clone_from    = var.vm_template_name
    //cloudinit = data.template_file.startup-script_data.rendered
    cpu_count   = var.vm_nb_cpu
    memory_size = var.vm_memory_size * 1024
    disk_size   = var.vm_system_disk_size
    datacenter  = var.vsphere_datacenter
    datastore   = var.vsphere_datastore
    //datastore_cluster = var.vsphere_cluster
    folder  = var.vsphere_folder // (Optional) vSphere folder for the docker VM. This folder must already exist in the datacenter (string)
    network = var.vsphere_network
    pool    = var.vsphere_resource_pool

  }
}

# Create a new rancher v2 vSphere K3S Cluster v2
resource "rancher2_cluster_v2" "cluster_on_vsphere" {
  name                                     = var.cluster_name != "" ? var.cluster_name : "${var.cluster_type_prefix}-on-vsphere-${random_id.cluster_instance_id.hex}"
  kubernetes_version                       = var.k8s_version
  enable_network_policy                    = false
  default_cluster_role_for_project_members = "user"

  // default timeouts are 30 minutes
  timeouts {
    create = "10m"
    update = "10m"
    delete = "10m"
  }

  // The map of Kubernetes labels to be applied to the cluster
  labels = var.labels

  local_auth_endpoint {
    ca_certs = ""    // (Optional) CA certs for the authorized cluster endpoint (string)
    enabled  = false // Enable the authorized cluster endpoint. Default true (bool)
    fqdn     = ""    // (Optional) FQDN for the authorized cluster endpoint (string)
  }

  rke_config {
    machine_pools {
      name                         = "pool1"
      cloud_credential_secret_name = rancher2_cloud_credential.vsphere_creds.id
      control_plane_role           = true
      etcd_role                    = true
      worker_role                  = true
      quantity                     = var.vm_count
      machine_config {
        kind = rancher2_machine_config_v2.vsphere.kind
        name = rancher2_machine_config_v2.vsphere.name
      }
    }
  }

  //depends_on = [rancher2_cloud_credential.vsphere_creds]
}

// We need to pass the Rancher registration command into the startup script above,
// so we need a data template which allows us to substitute Terraform variables in local files:
/* data "template_file" "startup-script_data" {
  template = file("${path.module}/files/shell_scripts/startup-script.sh")
  vars = {
    registration_command = var.insecure_rancher_server ? "${rancher2_cluster_v2.cluster_on_vsphere.cluster_registration_token.0.insecure_node_command} --etcd --controlplane --worker" : "${rancher2_cluster_v2.cluster_on_vsphere.cluster_registration_token.0.node_command}"
  }
  depends_on = [rancher2_cluster_v2.cluster_on_vsphere]
}
*/

locals {
  cluster_kubeconfig_filename = pathexpand("~/.kube/${rancher2_cluster_v2.cluster_on_vsphere.name}_config")
}

// Generate a kubeconfig file locally dedicated to the new cluster
resource "local_file" "cluster_kube_config" {
  content         = rancher2_cluster_v2.cluster_on_vsphere.kube_config
  filename        = local.cluster_kubeconfig_filename
  file_permission = "0600"
  depends_on      = [rancher2_cluster_v2.cluster_on_vsphere]
}