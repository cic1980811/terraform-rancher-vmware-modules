# Rancher settings
insecure_rancher_server = true

# VMware settings
vsphere_server        = "vcenter.lab.k8s.edge.inter-cdnrd.orange-business.com"
vsphere_user          = "<your-vshpere-login>"
vsphere_password      = "<your-vshpere-password>"
vsphere_datacenter    = "EdgeDC"
vsphere_folder        = "/my-folder"
vsphere_datastore     = "datastore1"
vsphere_resource_pool = "TF"
vsphere_network       = ["EDGE-VLAN"]

# VM settings
vm_template_name    = "<your-vm-template-name>"
vm_memory_size      = 2
vm_nb_cpu           = 2
vm_system_disk_size = 40

# K8S Cluster settings
labels              = { "provider" = "vsphere", "type" = "k3s" }
cluster_type_prefix = "k3s"
k8s_version         = "v1.22.7+k3s1"
vm_count            = 1
