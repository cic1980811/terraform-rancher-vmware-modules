variable "insecure_rancher_server" {
  description = "Flag that indicates whether the Rancher server uses an auto-signed certificate for TLS or not"
  type        = bool
  default     = true
}

variable "cluster_name" {
  description = "The name of the cluster to deploy. If you leave it empty, a default unique name will be built automatically"
  type        = string
  default     = ""
}

variable "labels" {
  description = "The map of Kubernetes labels to be applied to the cluster"
  type        = map(any)
  default = {
    "provider" = "vsphere",
    "type"     = "k3s"
  }
}

variable "k8s_version" {
  description = "The version of the Kubernetes cluster"
  type        = string
  default     = "v1.21.9-rancher1-1"
}

variable "cluster_type_prefix" {
  description = "The prefix to use for the type of cluster (used to build the name of the cluster)"
  type        = string
  default     = "k3s"
}

variable "vsphere_datacenter" {
  description = "The vSphere datacenter to use"
  type        = string
  default     = "dc1"
}

variable "vsphere_datastore" {
  description = "The vSphere datastore to use"
  type        = string
  default     = "datastore1"
}

variable "vsphere_folder" {
  description = "The absolute path of the folder for the VM. For example, given a default datacenter of `default-dc`, a folder of type `vm`, and a folder name of `terraform-test-folder`, the resulting path would be `/default-dc/vm/terraform-test-folder`."
  type        = string
  default     = "/dc1/vm/folder1"
}

variable "vsphere_resource_pool" {
  description = "The vSphere pool of resources to use"
  type        = string
  default     = "cluster1/Resources"
}

variable "vsphere_network" {
  description = "The vSphere network to use"
  type        = string
  default     = "public"
}

variable "vm_template_name" {
  description = "The name of the vSphere VM template to use in order to instantiate the virtual machine(s)."
  type        = string
}

variable "vm_count" {
  description = "number of VMs to create"
  type        = number
  default     = 1
  validation {
    condition     = var.vm_count >= 1
    error_message = "Must be 1 or more."
  }
}

variable "vm_iso_image" {
  description = "The type of operating system to deploy on the nodes of the cluster"
  type        = string
  default     = "/Volume/Storage/ISO/foo.iso"
}

variable "vm_memory_size" {
  description = "The size of the RAM on the virtual machine (in GBytes)"
  type        = number
  default     = 4
}

variable "vm_guest_os_id" {
  description = "The type of guest OS on the virtual machine"
  type        = string
  default     = "other3xLinux64Guest"
}

variable "vm_nb_cpu" {
  description = "The number of vcpu on the virtual machine"
  type        = number
  default     = 1
}

variable "vm_system_disk_size" {
  description = "The size of the system disk on the virtual machine (in GBytes)"
  type        = number
  default     = 20
}

/* variable "dns_server_list" {
  description = "The list of DNS servers to configure on VMs"
  type        = list(string)
  default     = ["192.168.1.57", "8.8.8.8"]
} */

variable "dns_server_1" {
  description = "The IP address of the first DNS server to configure on VMs"
  type        = string
  default     = "8.8.8.8"
}

variable "dns_server_2" {
  description = "The IP address of the second DNS server to configure on VMs"
  type        = string
  default     = "8.8.8.8"
}

variable "default_vm_user" {
  description = "The default shell user to create on VMs"
  type        = string
  default     = "local"
}

variable "default_vm_user_password" {
  description = "The default shell user to create on VMs"
  type        = string
  sensitive   = true
}

variable "vm_ssh_authorized_key" {
  description = "A public key to push on the VMs for SSH"
  type        = string
  sensitive   = true
}
