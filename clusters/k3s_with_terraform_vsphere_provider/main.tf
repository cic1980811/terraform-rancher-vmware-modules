
terraform {
  required_version = ">= 1.1.0"

  required_providers {
    rancher2 = {
      source  = "rancher/rancher2"
      version = "1.23.0"
    }

    vsphere = {
      source  = "hashicorp/vsphere"
      version = "2.1.1"
    }
  }
}

// To have a unique naming identifier for cluster and nodes, we use Random:
resource "random_id" "cluster_instance_id" {
  byte_length = 3
}

resource "vsphere_virtual_machine" "vm" {

  count            = var.vm_count
  name             = "${var.cluster_name}-vm-${count.index}"
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id     = data.vsphere_datastore.datastore.id

  // Tested but does not seem to work when using the id, must use the folder name instead !
  //folder = data.vsphere_folder.folder.id

  folder = var.vsphere_folder

  num_cpus  = var.vm_nb_cpu
  memory    = var.vm_memory_size * 1024
  guest_id  = data.vsphere_virtual_machine.template.guest_id
  scsi_type = data.vsphere_virtual_machine.template.scsi_type

  cdrom {
    client_device = true
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
    linked_clone  = false
  }

  network_interface {
    network_id   = data.vsphere_network.network.id
    #adapter_type = data.vsphere_virtual_machine.template.network_interface_types[0]
  }
  wait_for_guest_net_timeout = 0

  // Settings of the system disk
  disk {
    //label            = "${var.vm_name}.vmdk"
    label = "disk0"
    //label            = data.vsphere_virtual_machine.template.disks.0.label
    size = data.vsphere_virtual_machine.template.disks.0.size
    // Uncomment this if you want a system disk size different from the VM template
    // size        = var.vm_system_disk_size
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  vapp {
    properties = {
      hostname  = "${var.cluster_name}-vm-${count.index}"
      user-data = base64encode(data.template_file.cloud_init_kick_start.rendered)
    }
  }

  # NB this extra_config data ends-up inside the VM .vmx file and will be
  # exposed by cloud-init-vmware-guestinfo as a cloud-init datasource.
  extra_config = {
    "guestinfo.metadata"          = base64encode(data.template_file.cloud_init_metadata.rendered)
    "guestinfo.metadata.encoding" = "base64" # can be also "gzip+base64"
    "guestinfo.userdata"          = base64encode(file("${path.module}/files/cloudinit/userdata.yaml"))
    "guestinfo.userdata.encoding" = "base64"
  }

}

// We define the Rancher cluster, using the name from above and set Kubernetes networking and version:
resource "rancher2_cluster_v2" "cluster_on_vsphere" {
  name = var.cluster_name != "" ? var.cluster_name : "${var.cluster_type_prefix}-on-vsphere-${random_id.cluster_instance_id.hex}"

  //fleet_namespace                          = "fleet-default"
  kubernetes_version                       = var.k8s_version
  enable_network_policy                    = false
  default_cluster_role_for_project_members = "user"

  // The map of Kubernetes labels to be applied to the cluster
  labels = var.labels

  local_auth_endpoint {
    ca_certs = ""    // (Optional) CA certs for the authorized cluster endpoint (string)
    enabled  = false // Enable the authorized cluster endpoint. Default true (bool)
    fqdn     = ""    // (Optional) FQDN for the authorized cluster endpoint (string)
  }

  /*   rke_config {
    machine_pools {
      name = "pool1"
      cloud_credential_secret_name = rancher2_cloud_credential.google_creds.id
      control_plane_role = true
      etcd_role = true
      worker_role = true
      quantity = var.numnodes
       machine_config {
        kind = rancher2_machine_config_v2.foo.kind
        name = rancher2_machine_config_v2.foo.name
      }
    }
  } */

}

// We need to pass the Rancher registration command into the startup script above,
// so we need a data template which allows us to substitute Terraform variables in local files:
data "template_file" "cloud_init_kick_start" {
  template = file("${path.module}/files/cloudinit/kickstart.yaml.tftpl")
  vars = {
    registration_command     = var.insecure_rancher_server ? "${rancher2_cluster_v2.cluster_on_vsphere.cluster_registration_token.0.insecure_node_command} --etcd --controlplane --worker" : "${rancher2_cluster_v2.cluster_on_vsphere.cluster_registration_token.0.node_command}"
    default_vm_user          = var.default_vm_user
    default_vm_user_password = var.default_vm_user_password
    vm_ssh_authorized_key    = var.vm_ssh_authorized_key
  }
  depends_on = [rancher2_cluster_v2.cluster_on_vsphere]
}

data "template_file" "cloud_init_metadata" {
  template = file("${path.module}/files/cloudinit/metadata.yaml.tftpl")
  vars = {
    dns_server_1 = var.dns_server_1
    dns_server_2 = var.dns_server_2
  }
  depends_on = [rancher2_cluster_v2.cluster_on_vsphere]
}

locals {
  cluster_kubeconfig_filename = pathexpand("~/.kube/${rancher2_cluster_v2.cluster_on_vsphere.name}_config")
}

// Generate a kubeconfig file locally dedicated to the new cluster
resource "local_file" "cluster_kube_config" {
  content         = rancher2_cluster_v2.cluster_on_vsphere.kube_config
  filename        = local.cluster_kubeconfig_filename
  file_permission = "0600"
  depends_on      = [rancher2_cluster_v2.cluster_on_vsphere]
}