# Rancher settings
insecure_rancher_server = true

# VMware settings
vsphere_datacenter    = "EdgeDC"
vsphere_datastore     = "DS"
vsphere_resource_pool = "TF"
vsphere_network       = "EDGE-VLAN"

# VM settings
vm_count                 = 1
vm_iso_image             = "CentOS-8.3.2011-x86_64-dvd1.iso"
vm_guest_os_id           = "other3xLinux64Guest"
vm_memory_size           = 2
vm_nb_cpu                = 2
vm_system_disk_size      = 40
dns_server_1             = "192.168.1.57"
dns_server_2             = "8.8.8.8"
default_vm_user          = "local"
default_vm_user_password = "your-vm-password"
vm_ssh_authorized_key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDL2vwT429rXB4OYPVN2NHPIewRbSBdC0Cy5oMlKnSCzhAwh8Ug783NYbsEsrlXXxVhkpjiC7WpzPLWnsSRDv5XaaP1p0heCwrr+ai0kW0RydjK20LV3LK8v0R8S/ukm36cVo8oBQABZaVwpOUzjTpzUw6LSa+ybaQch96pj427xBCLhLQDJIk4LVqKx+kUGFDHzXxkJULreIVBdk1L1dCNbmUwxTyMqlPcX0jdqJ5vsLc13CLJ4Q48oROVy3OrtW3zLAu3PYnGnFoQgltG3GMdcR0T4x0ozEknHqKnE2cOlZ63dybnDDNR2AP/C5tlglLSVpNxzEs5UerBZR6zTko+kK5hB45Fy+UJn2L+Pc648/5Az0ufATVzdreWoyeXdj1mD8Ke37L8yt9uqQioZ8G5sCi4HDWYfmpR17MCugBDrhRuM3F71eYnCUaMkMHqcmV2PGI2PQ1ri/jwAb3kgkLrEfFg6fOT/cGDCxKp09hvp7CJBO6X7vGi/ti0uQSo/IHHQHbJ3Iro4PC5uuHEAp+aBDw2KPWIDX5SHAz3+qdXyCNLpjVZ7uNgmLc4buqTUrzKh0aGqkbbPeaYeQNaJBmC9OvFN2rSoHoCLhJ0hkdqUtqVBudYb76/KiLGugftM+M5wQW/vINh64BqPscj2/oAEsR5CHcsZLOQDGxLGbVzQQ== vagrant@linux"

# K8S Cluster settings
labels              = { "provider" = "vsphere", "type" = "k3s" }
cluster_type_prefix = "k3s"
k8s_version         = "v1.22.9+k3s1"
