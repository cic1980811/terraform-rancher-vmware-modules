# Deploying some Rancher clusters on VMWare

This project provides a list of Terraform modules that enable you to easily deploy different types of Rancher clusters on VMware Virtual Machines:

- K3S or RKE2 clusters
- RKE clusters (ongoing work, not tested yet)

It relies on Terraform and the official [Rancher2 provider](https://registry.terraform.io/providers/rancher/rancher2/1.22.2).

## Module 1: clusters-k3s-with-terraform-vsphere-provider

The first module is named `clusters-k3s-with-terraform-vsphere-provider` and it uses the [Terraform vsphere provider](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs) to instantiate the virtual machine on the vCenter server. This diagram shows the current architecture for this module:

![](img/technical_architecture_diag.drawio.png)

It is useful whenever your Rancher server can not directly access to vCenter APIs.

### Pre-conditions

You need:

- a Rancher v2 server
- a vCenter server (v6 or v7)
- an admin workstation:
  - equiped with Terraform
  - with a network access to a Rancher server APIs
  - with a network access to a vCenter server APIs
- a VM template created from an OVF/OVA file

### Create a VM template from OVF/OVA

See instructions for your OS on [this page](./Create_VM_Template.md).

At the end, to make sure that userdata & metadata on the terraformed VMs are applied instead of the initial OVF cloud-init configuration, **don’t forget to delete the content under `/var/lib/cloud/` on running VM named `ubuntu-21.04-server-cloudimg-amd64`** before running the `terraform apply` command again. This force cloud-init to re-run the new config even if the VM already booted.

You can now shut down the VM named `ubuntu-21.04-server-cloudimg-amd64` (**but don't restart it, otherwise, the `/var/lib/cloud/` folder will be populated again !**).

### Customize the VMs with cloud-init

It is possible to customize the VMs configuration by setting the appropriate [cloud-init modules settings](https://cloudinit.readthedocs.io/en/latest/topics/modules.html) in the following files:

- [kickstart.yaml.tftpl](clusters/k3s_with_terraform_vsphere_provider/files/cloudinit/kickstart.yaml.tftpl)
- [metadata.yaml.tftpl](clusters/k3s_with_terraform_vsphere_provider/files/cloudinit/metadata.yaml.tftpl)

Once instanciated from the OVF/OVA template (for example, named `ubuntu-21.04-server-cloudimg-amd64`), the VM will be customized via a cloud-init script and it will rebooted once (just after the end of executiong this cloud-init script).

It is possible to execute extra shell commands and post-configuration after this first reboot, by configuring the following file:

- [userdata.yaml](clusters/k3s_with_terraform_vsphere_provider/files/cloudinit/userdata.yaml)

You can troubleshoot the cloud-init by looking at the files in folder `/var/lib/cloud/instances/` of the VM and especially the following files:

- `/var/lib/cloud/instances/id-ovf/user-data.txt` that contains the generated cloud-init for first boot.
- `/var/lib/cloud/instances/<uuid>/user-data.txt` that contains the generated cloud-init for second boot.

Note that the instance id `id-ovf` can be configured in the initial OVF/OVA `vApp Options`:

![](img/vapp_instance_id.png)

## Module 2: clusters-k3s_with-rancher-vsphere-connector

The second module is named `clusters-k3s_with-rancher-vsphere-connector` and it uses the built-in vSphere connector of Rancher to instantiate.

This diagram shows the current architecture for this module:

![](img/target_technical_architecture_diag.drawio.png)

### Pre-conditions

You need:

- a Rancher v2 server
- a vCenter server (v6 or v7)
- make sure that the Rancher server can access to the vCenter APIs
- a template of virtual machine, previously created on the vCenter. For Ubuntu OS, you can get an image ready for VMware here <https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.vmdk>

## Initialize your VMware settings

You need a VMware pool of resources on your vCenter.

You also need to create a user account on the vCenter that can instantiate virtual machines (in this pool of resources) that can be plugged to a vSphere network from which it is possible to access Internet and especially the Rancher server APIs.

## Get Rancher credentials

You need also to create some Rancher access & secret key pair :

1. In the Rancher dashboard, select in the top-right menu, the item named `Account & API Keys`
2. Click on the `Create API Key` button. You may indicate an expiration date for those Rancher credentials.
3. Download the file containing the Rancher 2 credentials and configure the values of variables `rancher2_access_key` and `rancher2_secret_key` like in the sample file `terraform.example.tfvars` (that you should copy as a `terraform.tfvars` file)

## Configure your specific VM and cluster settings

Update Terraform variables related to the virtual machine(s) and K3S/RKE2 settings (node flavor, K8S version, ...) in the `terraform.tfvars`.

You can use the sample file `terraform.example.tfvars` to help you create the initial file.

Note that you can deploy a multiple nodes cluster by setting the `numnodes` variable.

## Deploy

- init terraform
```shell
terraform init
```
- run `terraform plan` to see the modifications:
```shell
terraform plan -var-file="terraform.tfvars"
```
- run `terraform apply` to apply the modifications:
```shell
terraform apply -var-file="terraform.tfvars"
```

## Get kubectl access to one of the clusters

A kubeconfig file is automatically generated in folder `~/.kube/` with name `<cluster-name>_config` for each cluster that you deploy.

You can also retrieve your kubeconfig from the output `k3s_cluster_kube_config`:

```shell
terraform output -raw k3s_cluster_kube_config
```

Generate an exported variable that contains a merged version of current kubeconfig with the newly created one:

```shell
export KUBECONFIG=~/.kube/config:~/.kube/<cluster-name>_config 
```

Check that the new K8S kubeconfig context has been added:

```shell
kubectl config view
kubectl config get-contexts
```

Then you can switch to the new K8S cluster context: `kubectl config use-context <context-name>`

And verify that you can fetch the list of pods running on the cluster: `kubectl get po --all-namespaces`

## To generate a new version of the Terraform modules

Just put a version tag on this git repo and the Terraform modules will be automatically rebuilt by Gitlab-CI pipeline with this version and pushed to the [Infrastructure Registry](https://gitlab.tech.orange/edge-services-env/ci-cd/terraform-rancher-vmware-modules/-/infrastructure_registry) of this Gitlab project.

```shell
git tag 1.0.0
git push origin 1.0.0
```

Then you can click on a given module to get the *Provision instructions* for using those newly built modules.

## Next steps

### New features

### Remaining bugs

- When instantiating several VMs, all the VMs have the same IP address. It may be related to the current IP allocation scheme that is configured in the `vApp options` of the `ubuntu-21.04-server-cloudimg-amd64` built from an official Ubuntu OVA image. See [this page](https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vsphere.vm_admin.doc/GUID-C0438F6D-9602-48BF-9FBA-B0B726E8A738.html).

#### Error on first launch

The first time you launch the terraform script, there is an error that is raised regarding the creation of the Rancher cloud credentials for accessing the vCenter server:

```
Error: Provider produced inconsistent final plan

When expanding the plan for module.k3s_cluster.rancher2_cluster_v2.cluster_on_vsphere to include new values learned so far during apply, provider "registry.terraform.io/rancher/rancher2" produced an invalid new value for .rke_config[0].machine_pools[0].cloud_credential_secret_name: was cty.StringVal(""), but now cty.StringVal"cattle-global-data:cc-dc78q").

This is a bug in the provider, which should be reported in the provider's own issue tracker.
```

You must ignore this error and relaunch `terraform apply` a second time and should be OK.

An issues has been submitted for that: <https://github.com/rancher/terraform-provider-rancher2/issues/915>

#### Error when setting the `vsphere_folder`

When setting [`vsphere_folder` input parameter](https://registry.terraform.io/providers/rancher/rancher2/latest/docs/resources/machine_config_v2#vsphere_config) in module `clusters-k3s_with-rancher-vsphere-connector`, it always adds a prefix even if you give an absolute path.

For example, if you set the `vsphere_folder` value to `/ESXi/vm`, it will use this folder path: `/ESXi/vm/ESXi/vm` and therefore trigger an error because this folder does not exist.

An issues has been submitted for that: <https://github.com/rancher/terraform-provider-rancher2/issues/914>

#### Bad settings for `clone_from`

In the Terraform rancher provider, the doc of [`rancher2_machine_config_v2` Resource](https://registry.terraform.io/providers/rancher/rancher2/latest/docs/resources/machine_config_v2#vsphere_config) says that `clone_from` can only be used when parameter `creation_type` parameter is set to `vm`. But it also works when `creation_type` is set to `template`.

An issue has been submitted for that: <https://github.com/rancher/terraform-provider-rancher2/issues/913>
